<?php 
	if(isset($_GET['error'])){
		error_message($_GET['error']);
	}
	if(isset($_GET['success'])){
		success_message($_GET['success']);
	}
?>

<?php 
	function error_message ($error){
		echo '<p class="alert alert-danger text-center">'.$error.'</p>';
	}
	function success_message($success){
		echo '<p class="alert alert-success text-center">'.$success.'</p>';
	}
?>