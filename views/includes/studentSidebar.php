 <div class="sidebar-menu">
    <header class="logo">
       <span id="logo"> <h4 style="color:white"><center>ICT 141 Class Record</center></h4></span>  
    </header>
    
    <div class="menu">
        <ul id="menu" >
            <li><a href="home.php"><i class="fa fa-home"></i> <span>home</span></a></li>
            
            <li><a href="grade.php"><i class="fa fa-user"></i> <span>Grades</span></a></li>

            <li><a href="schedule.php"><i class="fa fa-file-text-o"></i> <span>Schedule</span></a></li>

            <li><a href="logout.php"><i class="fa fa-arrow-left"></i> <span>Logout</span></a></li>

        </ul>
    </div>
</div>