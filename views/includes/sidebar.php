 <div class="sidebar-menu">
    <header class="logo">
       <span id="logo"> <h4 style="color:white"><center>ICT 141 Class Record</center></h4></span>  
    </header>
    
    <div class="menu">
        <ul id="menu" >
            <li><a href="student.php"><i class="fa fa-user"></i> <span>Students</span></a></li>

            <li><a href="subject.php"><i class="fa fa-table"></i> <span>Subjects</span></a></li>

            <li><a href="class.php"><i class="fa fa-file-text-o"></i> <span>Class</span></a></li>

            <li><a href="logout.php"><i class="fa fa-arrow-left"></i> <span>Logout</span></a></li>

        </ul>
    </div>
</div>