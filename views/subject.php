<?php require('../controllers/connection.php'); 

$conn = connect();

$sql = "SELECT * FROM subjects";
$result = mysqli_query($conn,$sql);

?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Client Management Sysytem || Manage Client </title>
			<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
			<link href="css/style.css" rel='stylesheet' type='text/css' />
			<link href="css/font-awesome.css" rel="stylesheet"> 
			<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
			<script src="js/jquery-1.10.2.min.js"></script>
	</head> 
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="inner-content">
				<?php include_once('includes/header.php');?>
				<div class="outter-wp">
					<div class="graph-visual tables-main">
						<h3 class="inner-tittle two">Manage Subjects </h3>
							<div class="tables">
								<table class="table" border="1">
										 <thead> 
										 	<tr> 
											 <th>Subject Code</th> 
											 <th>Subject Name</th>
											 <th>Options</th>
										   </tr>
										  </thead>
									    <tbody>
									    	<?php 
									    	while($row = mysqli_fetch_assoc($result)) {

									    	echo "<tr>";
									    		echo "<td>{$row['subject_code']}</td>";
									    		echo "<td>{$row['subject_name']}</td>";

									    		echo "<td>";
									    			echo "<a class='btn btn-primary btn-sm' href='edit_subject.php?edit={$row['subject_id']}'>Edit</a>";
									    			echo "<a class='btn btn-danger btn-sm' href='../controllers/subject_delete.php?del={$row['subject_id']}'>Delete</a>";
									    		echo "</td>";

									    	echo "</tr>";
									    	 }
									    	?>	
									    </tbody>
								</table>
							</div>

							<a href="add_subject.php" class="btn btn-success btn-sm">Add Subject</a>
					</div>
				</div>
				<?php include_once('includes/footer.php');?>
			</div>
		</div>
		<?php include_once('includes/sidebar.php');?>
		<div class="clearfix"></div>		
	</div>
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
