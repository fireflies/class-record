<!DOCTYPE HTML>
	<html>
		<head>
			<title>ICT 141 Class Record</title>
			<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
			<link href="css/style.css" rel='stylesheet' type='text/css' />
			<link href="css/font-awesome.css" rel="stylesheet"> 
			<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
			<script src="js/jquery-1.10.2.min.js"></script>
			<script src="js/css3clock.js"></script>
			<script src="js/skycons.js"></script>
		</head> 
	<body>

	<div class="page-container">
		<div class="left-content">
			<div class="inner-content">
		<?php include_once('includes/header.php');?>

		<?php 

		require("../controllers/connection.php");

		$conn = connect();

		$get = $_GET['edit'];
		$sql = "SELECT * FROM subjects WHERE subject_id = $get";
		$result = mysqli_query($conn,$sql);
		$row = mysqli_fetch_assoc($result);

		$sub_code = $row['subject_code'];
		$sub_name = $row['subject_name'];

		 ?>

	<div class="outter-wp">		
		<div class="forms-main">
			<h2 class="inner-tittle">Edit Subject</h2>
				<div class="graph-form">
					<div class="form-body">
						<form action="../controllers/subject_edit.php" method="POST"> 


			<input type="hidden" name="getid" value="<?php echo $get ?>">

			<div class="form-group">
				 <label>Subject Code</label> 
			 		<input type="text" name="sub_code" value="<?php echo $sub_code ?>" class="form-control" required='true'>
			  </div>

			  <div class="form-group">
				 <label>Subject Name</label> 
			 		<input type="text" name="sub_name" value="<?php echo $sub_name ?>" class="form-control" required='true'>
			  </div>
		
			 <button type="submit" class="btn btn-default" name="submit" id="submit">Submit</button>
			 <a href="subject.php" class="btn btn-danger">Cancel</a> 
				</form> 
			</div>
		</div>
	</div> 
</div>

		<?php include_once('includes/footer.php');?>
	</div>
</div>

		<?php include_once('includes/sidebar.php');?>
			<script src="js/jquery.nicescroll.js"></script>
			<script src="js/scripts.js"></script>
			<script src="js/bootstrap.min.js"></script>
	</body>
</html>
