<?php require('../controllers/connection.php'); 

$conn = connect();

$sql = "SELECT * FROM student";
$result = mysqli_query($conn,$sql);

$class = $_GET['class'];

?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Client Management Sysytem || Manage Client </title>
			<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
			<link href="css/style.css" rel='stylesheet' type='text/css' />
			<link href="css/font-awesome.css" rel="stylesheet"> 
			<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
			<script src="js/jquery-1.10.2.min.js"></script>
	</head> 
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="inner-content">
				<?php include_once('includes/header.php');?>
				<div class="outter-wp">
					<div class="graph-visual tables-main">
						<h3 class="inner-tittle two">Enroll Student </h3>

							<div class="tables">
								<table class="table" border="1">
										 <thead> 
										 	<tr> 
											 <th>Student ID Number</th>
											 <th>Student First Name</th> 
											 <th>Student Last Name</th>
											 <th>Action</th>
										   </tr>
										  </thead>
									    <tbody>
									    	<?php 
									    	while($row = mysqli_fetch_assoc($result)) {
									    	$id = $row['student_id'];
									    	$sql = "SELECT * FROM enroll_students WHERE student_id = '$id' ";
									    	$query = mysqli_query($conn, $sql);
									    	$result1 = mysqli_fetch_assoc($query);

									    	if($result1['student_id'] == $row['student_id']){

									    	}else{
										    	echo "<tr>";
										    		echo "<td>{$row['student_idnum']}</td>";
										    		echo "<td>{$row['student_fname']}</td>";
										    		echo "<td>{$row['student_lname']}</td>";

										    		echo "<td>";
										    			echo "<a class='btn btn-info btn-sm' href='../controllers/student_enroll.php?class={$class}&enroll={$row['student_id']}'>Enroll</a>";
										    		echo "</td>";

										    	echo "</tr>";
									    	}

									    	 }
									    	?>	
									    </tbody>
								</table>
							</div>

							<?php
								echo "<a href='view_student.php?class={$class}' class='btn btn-primary btn-sm'>Back</a>";
							?>
							
					</div>
				</div>
				<?php include_once('includes/footer.php');?>
			</div>
		</div>
		<?php include_once('includes/sidebar.php');?>
		<div class="clearfix"></div>		
	</div>
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
