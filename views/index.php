<!DOCTYPE HTML>
<html>
<head>
	<title>ICT 141 Class Record</title>
		<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
		<script src="js/jquery-1.10.2.min.js"></script>
</head> 

<body>
	<div class="error_page">

		<div class="error-top">
			<div class="login">
				
				<div class="buttons login">
					<h3 class="inner-tittle t-inner" style="color: black">ICT 141 Class Record</h3>
				</div>
				<?php require '../controllers/MessageController.php' ?>
				<form action="../controllers/login.php" method="POST" id="login">
					<input type="text" class="text" placeholder="Username" name="username" required="true" style="background:none;padding:0.9em 1em 0.9em 1em">
					<input type="password" placeholder="Password" name="password" required="true" style="background:none;padding:0.9em 1em 0.9em 1em">
					<div class="submit"><input type="submit" value="Login Here" name="login" ></div>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>

	<div class="footer">
		<?php include_once('includes/footer.php');?>
	</div>
	
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>