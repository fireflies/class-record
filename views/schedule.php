<?php require('../controllers/connection.php'); 
session_start();
	if($_SESSION['user_id'] == null){
		header('Location: index.php');
	}
$conn = connect();
$id = $_SESSION['user_id'];
$sql = "SELECT * FROM schedule WHERE student_id = '$id'";
$result = mysqli_query($conn,$sql);
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Client Management Sysytem || Manage Client </title>
			<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
			<link href="css/style.css" rel='stylesheet' type='text/css' />
			<link href="css/font-awesome.css" rel="stylesheet"> 
			<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
			<script src="js/jquery-1.10.2.min.js"></script>
	</head> 
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="inner-content">
				<?php include_once('includes/header.php');?>
				<div class="outter-wp">
					<div class="graph-visual tables-main">
						<h3 class="inner-tittle two">View schedule</h3>
							<div class="tables">
								<table class="table" border="1">
										 <thead> 
										 	<tr> 
											 <th>Subject name</th>
											 <th>Time</th> 
										   </tr>
										  </thead>
									    <tbody>
									    	<?php 
									    	while($row = mysqli_fetch_assoc($result)) {

									    	echo "<tr>";
									    		echo "<td>{$row['schedule_subject']}</td>";
									    		echo "<td>{$row['schedule_time']}</td>";
									    	echo "</tr>";
									    	 }
									    	?>	
									    </tbody>
								</table>
							</div>
					</div>
				</div>
				<?php include_once('includes/footer.php');?>
			</div>
		</div>
		<?php include_once('includes/studentSidebar.php');?>
		<div class="clearfix"></div>		
	</div>
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
