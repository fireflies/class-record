<?php
	require("../controllers/connection.php");

	$conn = connect();

	$sql = "SELECT * FROM classes JOIN subjects ON classes.subject_id = subjects.subject_id";
	$result = mysqli_query($conn,$sql);


 ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>ICT 141 Class Record</title>
			<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
			<link href="css/style.css" rel='stylesheet' type='text/css' />
			<link href="css/font-awesome.css" rel="stylesheet"> 
			<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
			<script src="js/jquery-1.10.2.min.js"></script>
	</head> 
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="inner-content">
				<?php include_once('includes/header.php');?>
				<div class="outter-wp">
					<div class="graph-visual tables-main">
						<h3 class="inner-tittle two">Manage Class </h3>
							<div class="tables">
								<table class="table" border="1">
										 <thead> 
										 	<tr> 
												 <th>Subject Code</th>
												 <th>Subject Name</th>
												 <th>Teacher Full Name</th>
												 <th>Students</th>
												 <th>Options</th>
										   </tr>
										  </thead>
									    <tbody>
									    <?php
									    	while($row = mysqli_fetch_assoc($result)){
									    		echo "<tr>";
									    		echo "<td>{$row['subject_code']}</td>";
									    		echo "<td>{$row['subject_name']}</td>";
									    		echo "<td>{$row['fname']} {$row['lname']}</td>";

									    		echo "<td><a href='view_student.php?class={$row['class_id']}' class='btn btn-success'>View</a></td>";

									    		echo "<td>";
									    			echo "<a class='btn btn-primary' href='edit_class.php?edit={$row['class_id']}'>Edit</a>";
									    			echo "<a class='btn btn-danger' href='../controllers/class_delete.php?del={$row['class_id']}'>Delete</a>";
									    		echo "</td>";
									    		echo "</tr>";

									    	}	
									     ?>	
									    </tbody>
								</table>
							</div>

							<a href="add_class.php" class="btn btn-success btn-sm">Add Class</a>
					</div>
				</div>
				<?php include_once('includes/footer.php');?>
			</div>
		</div>
		<?php include_once('includes/sidebar.php');?>
		<div class="clearfix"></div>		
	</div>
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>

