<?php
	session_start();
	if($_SESSION['user_id'] == null){
		$error = 'Your not logged in';
		header('Location: index.php?error='.$error);
	}

?>

<!DOCTYPE HTML>
<html>
<head>
	<title>ICT 141 Class Record</title>
		<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
		<script src="js/jquery-1.10.2.min.js"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head> 

<body>
<?php include_once('includes/header.php');?>

<?php
 if($_SESSION['user_type'] == '1'){
 	include_once('includes/sidebar.php');	
 }else{
 	include_once('includes/studentSidebar.php');
 }
 ?>
	<?php require '../controllers/MessageController.php' ?>
        
	<div class="footer">
		<?php include_once('includes/footer.php');?>
	</div>
	
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>