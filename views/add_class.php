<!DOCTYPE HTML>
	<html>
		<head>
			<title>ICT 141 Class Record</title>
			<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
			<link href="css/style.css" rel='stylesheet' type='text/css' />
			<link href="css/font-awesome.css" rel="stylesheet"> 
			<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
			<script src="js/jquery-1.10.2.min.js"></script>
			<script src="js/css3clock.js"></script>
			<script src="js/skycons.js"></script>
		</head> 
	<body>

	<div class="page-container">
		<div class="left-content">
			<div class="inner-content">
		<?php include_once('includes/header.php');?>

	<div class="outter-wp">		
		<div class="forms-main">
			<h2 class="inner-tittle">Add Class</h2>
				<div class="graph-form">
					<div class="form-body">
						<form action="../controllers/class_add.php" method="POST"> 

			<?php

			require('../controllers/connection.php');

			$conn = connect();

			$sql = "SELECT * FROM subjects";
			$result = mysqli_query($conn,$sql);

			 ?>
	
			<div class="form-group">
				 <label>Subject</label> 
			 		<select class="form-control" style="height:45px" name="subject">
			 			<option selected hidden>Select A Subject</option>
			 			<?php while($row = mysqli_fetch_assoc($result)){
			 				echo "<option value='{$row['subject_id']}'>{$row['subject_code']} - {$row['subject_name']} </option>";
			 			} ?>
			 		 </select>
			  </div>

			  <div class="form-group">
				 <label>Teacher First Name</label> 
			 		<input type="text" name="firstname" placeholder="Input First Name" class="form-control" required='true'>
			  </div>

			   <div class="form-group">
				 <label>Teacher Last Name</label> 
			 		<input type="text" name="lastname" placeholder="Input Last Name" class="form-control" required='true'>
			  </div>
		
	 <button type="submit" class="btn btn-default" name="submit" id="submit">Submit</button>
	 <a href="class.php" class="btn btn-danger">Cancel</a> 
				</form> 
			</div>
		</div>
	</div> 
</div>

		<?php include_once('includes/footer.php');?>
	</div>
</div>

		<?php include_once('includes/sidebar.php');?>
			<script src="js/jquery.nicescroll.js"></script>
			<script src="js/scripts.js"></script>
			<script src="js/bootstrap.min.js"></script>
	</body>
</html>
