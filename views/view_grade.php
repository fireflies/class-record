<?php require('../controllers/connection.php'); 

$conn = connect();

$id = $_GET['edit'];
$class = $_GET['class'];

$sql = "SELECT * FROM student WHERE student_id = '$id' ";
$query = mysqli_query($conn,$sql);
$result = mysqli_fetch_assoc($query);

$sql = "SELECT * FROM grades WHERE student_id = '$id' ";
$query = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($query);

if($row['premid'] == 0){
 	$premid = 0.0;
} else {
	$premid = $row['premid'];
}

if($row['mid'] == 0){
 	$mid = 0.0;
} else {
	$mid = $row['mid'];
}

if($row['prefi'] == 0){
 	$prefi = 0.0;
} else {
	$prefi = $row['prefi'];
}

if($row['final'] == 0){
 	$final = 0.0;
} else {
	$final = $row['final'];
}

if($row['premid'] != 0.0 && $row['mid'] != 0.0 && $row['prefi'] != 0.0 && $row['final'] != 0.0){
	
	$grade1 = $premid*0.20;
	$grade2 = $mid*0.20;
	$grade3 = $prefi*0.20;
	$grade4 = $final*0.40;

	$total_grade = $grade1 + $grade2 + $grade3 + $grade4;
	$grade = $total_grade;
}else{
	$grade = 0.0;
}

?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Client Management Sysytem || Manage Client </title>
			<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
			<link href="css/style.css" rel='stylesheet' type='text/css' />
			<link href="css/font-awesome.css" rel="stylesheet"> 
			<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
			<script src="js/jquery-1.10.2.min.js"></script>
	</head> 
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="inner-content">
				<?php include_once('includes/header.php');?>
				<div class="outter-wp">
					<div class="graph-visual tables-main">
						<h3 class="inner-tittle two">Grades For <?php echo $result['student_fname'].' '.$result['student_lname'] ?> </h3>
							<div class="row">
								<div class="col-md-3">
									<form action="../controllers/grade_add.php?class=<?php echo $class ?>&edit=<?php echo $id ?>" method="POST">
										<div class="form-group">
											<select class="form-control" name="grade" required style="height: 50px">
												<option selected hidden value="">Select</option>
												<option value="premid">Pre-Midterm</option>
												<option value="mid">Midterm</option>
												<option value="prefi">Pre-Final</option>
												<option value="final">Final</option>
											</select>
										</div>
										<div class="form-group">
											<label>Actual Score</label>
											<input type="number" required name="actual_score" class="form-control">
										</div>
										<div class="form-group">
											<label>Perfect Score</label>
											<input type="number" required name="perfect_score" class="form-control">
										</div>
								</div>
								<div class="col-md-9">									
									<div class="tables">
										<table class="table" border="1">
												 <thead> 
												 	<tr> 
													 <th>Pre-Midterm (20%)</th>
													 <th>Midterm (20%)</th> 
													 <th>Pre-Final (20%)</th>
													 <th>Final (%40)</th>
													 <th>Grade (100%)</th>
												   </tr>
												  </thead>
											    <tbody>
											    	<?php 
												    	echo "<tr>";
												    		echo "<td>".number_format($premid, 2)."</td>";
												    		echo "<td>".number_format($mid, 2)."</td>";
												    		echo "<td>".number_format($prefi, 2)."</td>";
												    		echo "<td>".number_format($final, 2)."</td>";
												    		echo "<td>".number_format($grade, 2)."</td>";

												    	echo "</tr>";
											    	?>	
											    </tbody>
										</table>
									</div>
								</div>
							</div>
							<button class="btn btn-info" type="submit" name="submit">Submit</button>
							<?php 
								echo "<a href='view_student.php?class={$class}' class='btn btn-primary btn-sm'>Back</a>";
							?>
						</form>
							
					</div>
				</div>
				<?php include_once('includes/footer.php');?>
			</div>
		</div>
		<?php include_once('includes/sidebar.php');?>
		<div class="clearfix"></div>		
	</div>
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
